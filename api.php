<?php

$uri = $_SERVER['REQUEST_URI'];

if ($uri == '/') {
    echo file_get_contents('index.html');
} elseif (preg_match('~^/api/items(/(?P<action>.+))?$~', $uri, $matches)) {
    $response = '';
    $action = 'list';

    if (isset($matches['action'])) {
        $action = $matches['action'];
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if ($action == 'add') {
            $item = [
                'id'   => $_POST['id'],
                'name' => $_POST['name'],
            ];

            $items = [];

            if (file_exists('items.json')) {
                $content = file_get_contents('items.json');
                $items = json_decode($content, true);
            }

            $items[] = $item;

            file_put_contents('items.json', json_encode($items));

            $response = json_encode(['result' => 0]);
        }
    } else {
        if (file_exists('items.json')) {
            $content = file_get_contents('items.json');

            $response = $content;
        }
    }

    header('Content-Type: application/json');
    header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: *');

    echo $response;
} elseif ($uri == '/api/items/add') {
} else {
    return false;
}
