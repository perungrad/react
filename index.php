<?php

require 'vendor/autoload.php';

$reactFiles = [
    'node_modules/react/dist/react.js',
    'node_modules/react-dom/dist/react-dom.js',
    'node_modules/react-dom/dist/react-dom-server.js',
];

$reactSrc = implode("\n", array_map(function ($src) {
    return file_get_contents($src);
}, $reactFiles));

$appSrc = file_get_contents('assets/js/dist/component/List.js');

$rjs = new ReactJS($reactSrc, $appSrc);

$rjs->setComponent('List', [
    'items' => [
        [
            'id'   => 1,
            'name' => 'fero',
        ]
    ],
]);

$markup = $rjs->getMarkup();
$jsCode = $rjs->getJS('#content');

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>abc</title>
</head>
<body>
    <div id='content'><?php echo $markup; ?></div>

    <script><?php echo $jsCode; ?></script>
</body>
</html>
