const gulp = require('gulp');
const babel = require('gulp-babel');

const paths = {
    jsSrc: 'assets/js/src/**/*.js',
    jsDest: 'assets/js/dist/',
};

gulp.task('babel', function () {
    return gulp.src(paths.jsSrc)
        .pipe(babel({
            presets: ['es2015', 'react']
        }))
        .pipe(gulp.dest(paths.jsDest));
});

gulp.task('watch', function () {
    gulp.watch(paths.jsSrc, ['babel']);
});

gulp.task('default', ['babel']);
