'use strict';

define(['dispatcher/dispatcher'], function (dispatcher) {
    return {
        create: function create(item) {
            dispatcher.dispatch('todoItemAdd', item);
        }
    };
});