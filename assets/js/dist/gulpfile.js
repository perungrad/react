'use strict';

var gulp = require('gulp');
var babel = require('gulp-babel');
var watch = require('gulp-watch');

gulp.task('default', function () {
    gulp.src('assets/js/src/**/*.js').pipe(babel({
        presets: ['es2015', 'react']
    })).pipe(gulp.dest('assets/js/dist/'));
});

gulp.task('watch', function () {
    gulp.src('assets/js/src/**/*.js').pipe(watch('assets/js/src/**/*.js')).pipe(babel({
        presets: ['es2015', 'react']
    })).pipe(gulp.dest('assets/js/dist/'));
});