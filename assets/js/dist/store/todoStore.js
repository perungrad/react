'use strict';

define(['jquery'], function ($) {
    var items = [],
        viewListeners = [],
        init,
        _add,
        _registerViewListener,
        _unregisterViewListener,
        updateViews,
        getSubscribedEvents,
        notify;

    init = function init() {
        $.ajax({
            url: '/api/items',
            type: 'GET',
            dataType: 'json',
            success: function success(data) {
                items = data;

                updateViews();
            }
        });
    };

    _add = function add(item) {
        if (!item.id) {
            item.id = Date.now();
        }

        items.push(item);

        updateViews();

        $.ajax({
            url: '/api/items/add',
            type: 'POST',
            dataType: 'json',
            data: {
                id: item.id,
                name: item.name
            },
            success: function success(data) {},
            error: function error() {
                items = items.filter(function (itm) {
                    return itm !== item;
                });

                updateViews();
            }
        });
    };

    getSubscribedEvents = function getSubscribedEvents() {
        return ['todoItemAdd', 'todoItemRemove', 'todoItemEdit'];
    };

    notify = function notify(event) {
        if (event.eventName === 'todoItemAdd') {
            _add(event.msg);
        }

        return true;
    };

    _registerViewListener = function registerViewListener(listener) {
        viewListeners.push(listener);
    };

    _unregisterViewListener = function unregisterViewListener(listener) {
        viewListeners = viewListeners.filter(function (eachListener) {
            return listener !== eachListener;
        });
    };

    updateViews = function updateViews() {
        viewListeners.map(function (listener) {
            listener();
        });
    };

    return {
        init: init,
        getAll: function getAll() {
            return items;
        },
        add: function add(item) {
            return _add(item);
        },
        registerViewListener: function registerViewListener(view) {
            return _registerViewListener(view);
        },
        unregisterViewListener: function unregisterViewListener(view) {
            return _unregisterViewListener(view);
        },
        getSubscribedEvents: getSubscribedEvents,
        notify: notify
    };
});