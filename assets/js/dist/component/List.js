"use strict";

var List = React.createClass({
    displayName: "List",

    render: function render() {
        var items = this.props.items.map(function (item) {
            return React.createElement(
                "li",
                { key: item.id, className: "list-group-item" },
                item.name
            );
        });
        return React.createElement(
            "ul",
            { className: "list-group" },
            items
        );
    }
});