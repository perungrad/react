'use strict';

define(['react', 'component/TaskInput', 'component/TaskList', 'store/todoStore', 'dispatcher/dispatcher', 'actions/todoActions'], function (React, TaskInput, TaskList, todoStore, dispatcher, todoActions) {
    return React.createClass({
        getInitialState: function getInitialState() {
            return {
                items: todoStore.getAll()
            };
        },
        componentDidMount: function componentDidMount() {
            todoStore.registerViewListener(this.onStoreChange);
        },
        componentWillUnmount: function componentWillUnmount() {
            todoStore.unregisterViewListener(this.onStoreChange);
        },
        addItem: function addItem(item) {
            todoActions.create(item);
        },
        onStoreChange: function onStoreChange() {
            this.setState({ items: todoStore.getAll() });
        },
        render: function render() {
            return React.createElement(
                'div',
                { className: 'panel panel-default' },
                React.createElement(
                    'div',
                    { className: 'panel-heading' },
                    React.createElement(
                        'h2',
                        { className: 'panel-title' },
                        'Todo ',
                        React.createElement(
                            'span',
                            { className: 'badge' },
                            this.state.items.length
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'panel-body' },
                    React.createElement(TaskInput, { onSave: this.addItem })
                ),
                React.createElement(TaskList, { items: this.state.items })
            );
        }
    });
});