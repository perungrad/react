'use strict';

define(['react'], function (React) {
    return React.createClass({
        propTypes: {
            onSave: React.PropTypes.func.isRequired,
            value: React.PropTypes.string
        },
        getInitialState: function getInitialState() {
            return {
                value: this.props.value || ''
            };
        },
        save: function save() {
            this.props.onSave({ name: this.state.value });

            this.setState({ value: '' });
        },
        onChange: function onChange(e) {
            this.setState({ value: e.target.value });
        },
        onKeyDown: function onKeyDown(e) {
            if (e.keyCode === 13) {
                this.save();
            }
        },
        render: function render() {
            return React.createElement(
                'div',
                null,
                React.createElement('input', { className: 'form-control', onChange: this.onChange, value: this.state.value, onKeyDown: this.onKeyDown })
            );
        }
    });
});