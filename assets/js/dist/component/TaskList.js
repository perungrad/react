"use strict";

define(['react'], function (React) {
    return React.createClass({
        render: function render() {
            var items = this.props.items.map(function (item) {
                return React.createElement(
                    "li",
                    { key: item.id, className: "list-group-item" },
                    item.name
                );
            });
            return React.createElement(
                "ul",
                { className: "list-group" },
                items
            );
        }
    });
});