"use strict";

define([], function () {
    var listeners = {},
        register,
        unregister,
        dispatch;

    register = function register(eventName, listener) {
        if (!listeners[eventName]) {
            listeners[eventName] = [];
        }

        listeners[eventName].push(listener);
    };

    unregister = function unregister(eventName, listener) {
        if (listeners[eventName]) {
            listeners[eventName] = listeners[eventName].filter(function (eachListener) {
                return eachListener !== listener;
            });
        }
    };

    dispatch = function dispatch(eventName, msg) {
        if (listeners[eventName]) {
            listeners[eventName].map(function (listener) {
                listener({
                    eventName: eventName,
                    msg: msg
                });
            });
        }
    };

    return {
        register: register,
        unregister: unregister,
        dispatch: dispatch
    };
});