define(['dispatcher/dispatcher'], function (dispatcher) {
    return {
        create: function (item) {
            dispatcher.dispatch('todoItemAdd', item);
        }
    };
});

