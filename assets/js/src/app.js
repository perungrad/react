require(['react', 'reactDOM', 'component/TodoApp', 'store/todoStore', 'dispatcher/dispatcher'], function (React, ReactDOM, TodoApp, todoStore, dispatcher) {
    todoStore.getSubscribedEvents().map(function (eventName) {
        dispatcher.register(eventName, todoStore.notify);
    });

    todoStore.init();

    ReactDOM.render(
        <TodoApp />,
        document.getElementById('todo-container')
    );
});
