define(['jquery'], function ($) {
    var items = [],
        viewListeners = [],
        init,
        add,
        registerViewListener,
        unregisterViewListener,
        updateViews,
        getSubscribedEvents,
        notify;

    init = function () {
        $.ajax({
            url: '/api/items',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                items = data;

                updateViews();
            },
        });
    };

    add = function (item) {
        if (!item.id) {
            item.id = Date.now();
        }

        items.push(item);

        updateViews();

        $.ajax({
            url: '/api/items/add',
            type: 'POST',
            dataType: 'json',
            data: {
                id: item.id,
                name: item.name,
            },
            success: function (data) {
            },
            error: function () {
                items = items.filter(function (itm) {
                    return itm !== item;
                });

                updateViews();
            }
        });
    };

    getSubscribedEvents = function () {
        return [
            'todoItemAdd',
            'todoItemRemove',
            'todoItemEdit',
        ];
    };

    notify = function (event) {
        if (event.eventName === 'todoItemAdd') {
            add(event.msg);
        }

        return true;
    };

    registerViewListener = function (listener) {
        viewListeners.push(listener);
    };

    unregisterViewListener = function (listener) {
        viewListeners = viewListeners.filter(function (eachListener) {
            return listener !== eachListener;
        });
    };

    updateViews = function () {
        viewListeners.map(function (listener) {
            listener();
        });
    };

    return {
        init,
        getAll: function () {
            return items;
        },
        add: function (item) {
            return add(item);
        },
        registerViewListener: function (view) {
            return registerViewListener(view);
        },
        unregisterViewListener: function (view) {
            return unregisterViewListener(view);
        },
        getSubscribedEvents,
        notify,
    };
});

