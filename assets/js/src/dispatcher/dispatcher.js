define([], function () {
    var listeners = {},
        register,
        unregister,
        dispatch;

    register = function (eventName, listener) {
        if (!listeners[eventName]) {
            listeners[eventName] = [];
        }

        listeners[eventName].push(listener);
    };

    unregister = function (eventName, listener) {
        if (listeners[eventName]) {
            listeners[eventName] = listeners[eventName].filter(function (eachListener) {
                return eachListener !== listener;
            });
        }
    };

    dispatch = function (eventName, msg) {
        if (listeners[eventName]) {
            listeners[eventName].map(function (listener) {
                listener({
                    eventName: eventName,
                    msg: msg,
                });
            });
        }
    };

    return {
        register,
        unregister,
        dispatch,
    };
});

