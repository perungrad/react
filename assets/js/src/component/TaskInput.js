define(['react'], function (React) {
    return React.createClass({
        propTypes: {
            onSave: React.PropTypes.func.isRequired,
            value: React.PropTypes.string,
        },
        getInitialState: function () {
            return {
                value: this.props.value || ''
            };
        },
        save: function () {
            this.props.onSave({name: this.state.value});

            this.setState({value: ''});
        },
        onChange: function (e) {
            this.setState({value: e.target.value});
        },
        onKeyDown: function (e) {
            if (e.keyCode === 13) {
                this.save();
            }
        },
        render: function () {
            return (
                <div>
                    <input className="form-control" onChange={this.onChange} value={this.state.value} onKeyDown={this.onKeyDown} />
                </div>
            );
        }
    });
});

