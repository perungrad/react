define(['react', 'component/TaskInput', 'component/TaskList', 'store/todoStore', 'dispatcher/dispatcher', 'actions/todoActions'], function (React, TaskInput, TaskList, todoStore, dispatcher, todoActions) {
    return React.createClass({
        getInitialState: function () {
            return {
                items: todoStore.getAll(),
            };
        },
        componentDidMount: function () {
            todoStore.registerViewListener(this.onStoreChange);
        },
        componentWillUnmount: function () {
            todoStore.unregisterViewListener(this.onStoreChange);
        },
        addItem: function (item) {
            todoActions.create(item);
        },
        onStoreChange: function () {
            this.setState({items: todoStore.getAll()});
        },
        render: function () {
            return (
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h2 className="panel-title">Todo <span className="badge">{this.state.items.length}</span></h2>
                    </div>
                    <div className="panel-body">
                        <TaskInput onSave={this.addItem} />
                    </div>
                    <TaskList items={this.state.items}/>
                </div>
            );
        }
    });
});

