define(['react'], function (React) {
    return React.createClass({
        render: function () {
            var items = this.props.items.map(function (item) {
                return (
                    <li key={item.id} className="list-group-item">
                        {item.name}
                    </li>
                );
            });
            return (
                <ul className="list-group">
                    {items}
                </ul>
            );
        }
    });
});
